# Summary
This repository is source of https://hekkaryk.eu/ and https://hekkaryk.pl/  
At a moment no contributions other than bug reports and suggestions are accepted.  
Primary objective of project is personal, Internet-visible repository of data easily available from both graphical web browsers such as `Google Chrome` and `Mozilla Firefox` and terminal web browsers such as `links` and `elinks`.
