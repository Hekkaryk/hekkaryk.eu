window.onload = hk_load_kh_config;
function hk_load_kh_config() {
    try {
        getLocation();
        handleHkConfig(href_after_hash);
        console.log("Reported SHA512 checksum of hk.js: " + document.getElementById("hk_js").src.split('=')[1] + "\nReported SHA512 checksum of hk.css: " + document.getElementById("hk_css").href.split('=')[1]);
    } catch (e) {
        console.error("Exception in hk_load_kh_config", e);
    }
}
function handleHkConfig(href){
    if(href.indexOf("hk_config_full_config_div") !== -1)
        toggleTabInner("hk_config_options", "#hkconfig,hk_config_full_config_div");
    document.getElementById("black_screen").classList.add("showing");
}
let hk_config_input = document.getElementsByClassName("hk_config_input");
let hk_config_output = document.getElementsByClassName("hk_config_output");
for (let i = 0; i < hk_config_input.length; i++) {
    hk_config_input[i].addEventListener('change', function () { updateMatchingHkConfigOutput(hk_config_input[i]) });
    updateMatchingHkConfigOutput(hk_config_input[i]);
}
document.getElementById("hk_config_copy").addEventListener('click', function () {
    regenerateHkConfig();
    copyToClipboardFromId("hk_config_full_config");
});
function updateMatchingHkConfigOutput(input) {
    let value = input.value;
    let field = input.dataset.field;
    for (let i = 0; i < hk_config_output.length; i++) {
        if (hk_config_output[i].dataset.field === field) {
            if (field === "[Hk]Config++")
                hk_config_output[i].innerText = "\"" + field + "\"=";
            else
                hk_config_output[i].innerText = field + "=";
            if (input.type === "number" || input.dataset.type === "number")
                hk_config_output[i].innerText = hk_config_output[i].innerText + value + ",";
            else
                hk_config_output[i].innerText = hk_config_output[i].innerText + "\"" + value + "\",";
            break;
        }
    }
    regenerateHkConfig();
}
let hk_config_full_config = document.getElementById("hk_config_full_config");
let hk_config_full_config_refreshing = false;
function regenerateHkConfig() {
    try {
        if (hk_config_full_config_refreshing)
            return;
        let value = "";
        for (let i = 0; i < hk_config_output.length - 1; i++) {
            value = value + hk_config_output[i].innerText + "\n";
        }
        let last = hk_config_output[hk_config_output.length - 1].innerText;
        value = value + last.replace(",", "");
        hk_config_full_config_refreshing = true;
        hk_config_full_config.innerText = value;
        hk_config_full_config.value = value;
        hk_config_full_config_refreshing = false;
    } catch (e) {
    }
}
hk_config_full_config.addEventListener('load', regenerateHkConfig);
document.getElementById("hk_config_import").addEventListener('change', function () { hk_config_import_fn(); });
function hk_config_import_fn() {
    const file = document.getElementById("hk_config_import").files[0];
    const reader = new FileReader();
    reader.onloadend = function () {
        hk_config_read_from_file(reader.result);
    };
    reader.readAsText(file);
}
function hk_config_read_from_file(text) {
    let lines = text.split('\n');
    for (let i = 0; i < lines.length; i++) {
        let index = lines[i].indexOf('=');
        let name = lines[i].substring(0, index);
        let value = lines[i].substring(index + 1, lines[i].length);
        if (value.charAt(value.length - 1) === ',')
            value = value.substring(0, value.length - 1);
        if (value.charAt(value.length - 1) === '\"')
            value = value.substring(0, value.length - 1);
        if (value.charAt(0) === '\"')
            value = value.substring(1);
        for (let x = 0; x < hk_config_input.length; x++) {
            if (hk_config_input[x].dataset.field === name) {
                hk_config_input[x].value = value;
                break;
            }
        }
    }
    importHkConfig();
}
function importHkConfig() {
    hk_config_full_config_refreshing = true;
    for (let i = 0; i < hk_config_input.length; i++) {
        updateMatchingHkConfigOutput(hk_config_input[i]);
    }
    hk_config_full_config_refreshing = false;
    regenerateHkConfig();
}