// noinspection JSUnusedGlobalSymbols

function handleRopHref(href){
    let parts = href.split(',');
    for (let i = 0; i < parts.length; i++) {
        let foo = parts[i].split('=');
        if (foo.length !== 2) continue;
        let bar = foo[0].split('?');
        console.log("rop loads", bar);
        let region = bar.length > 1 ? bar[1] : bar[0];
        if (region === "north")
            replay_map(games_rop_north_map, "rop_tile", foo[1], rop_north_seq, "north");
        else if (region === "south")
            replay_map(games_rop_south_map, "rop_tile_south", foo[1], rop_south_seq, "south");
    }
    if (href.indexOf("games_rop_south") !== -1)
        toggleTabInner("games_rop_planners", "#planner,games_rop_south");
    document.getElementById("black_screen").classList.add("showing");
}
let initialized = false;
let games_rop_status = document.getElementById("games_rop_status");
let rop_sel_str_name = "rop_tile_pump";
document.getElementById("rop_pump").addEventListener('click', function () { rop_select_structure("rop_tile_pump") });
document.getElementById("rop_trench").addEventListener('click', function () { rop_select_structure("rop_tile_trench") });
document.getElementById("rop_extender").addEventListener('click', function () { rop_select_structure("rop_tile_extender") });
function rop_select_structure(structure) {
    rop_sel_str_name = structure;
    let name = "Irrigation Trench";
    if (structure === "rop_tile_pump") name = "Irrigation Pump"; else if (structure === "rop_tile_extender") name = "Irrigation Extender";
    document.getElementById("games_rop_structure").innerText = "Selected building: " + name;
}
rop_select_structure("rop_tile_pump");
let games_rop_north_map_scheme = [
    [1, 25],
    [2, 13, 7, 5],
    [2, 13, 11, 1],
    [4, 8, 16, 1],
    [2, 4, 20, 1],
    [7, 1, 23, 1],
    [5, 8, 16, 1],
    [1, 25]
];
let games_rop_north_map = [];
let games_rop_north = document.getElementById("games_rop_north");
let games_rop_north_table = document.createElement("table");
games_rop_north.append(games_rop_north_table);
rop_initialize_map(games_rop_north_map, games_rop_north_map_scheme, games_rop_north_table, "rop_tile", "north");
let games_rop_south_map_scheme = [
    [1, 60],
    [4, 18, 22, 20],
    [18, 13, 27, 20],
    [5, 2, 38, 20],
    [10, 1, 39, 20],
    [1, 1, 58, 1],
    [6, 2, 57, 1],
    [3, 1, 58, 1],
    [4, 2, 57, 1],
    [1, 1, 57, 2],
    [1, 2, 52, 6],
    [1, 3, 47, 10],
    [1, 4, 42, 14],
    [1, 5, 37, 18],
    [1, 5, 31, 24],
    [1, 5, 29, 26],
    [1, 6, 23, 31],
    [1, 8, 14, 38],
    [1, 10, 11, 39],
    [1, 11, 7, 42],
    [1, 60]
];
let games_rop_south_map = [];
let games_rop_south = document.getElementById("games_rop_south");
let games_rop_south_table = document.createElement("table");
games_rop_south.append(games_rop_south_table);
rop_initialize_map(games_rop_south_map, games_rop_south_map_scheme, games_rop_south_table, "rop_tile_south", "south");
function rop_initialize_map(map, map_scheme, map_table, base_class, map_name) {
    let xx = 0;
    for (let x = 0; x < map_scheme.length; x++) {
        for (let c = 0; c < map_scheme[x][0]; c++) {
            map[xx] = [];
            let workable = false;
            let row = document.createElement("tr");
            map_table.append(row);
            let i = 0;
            for (let y = 1; y < map_scheme[x].length; y++) {
                for (let z = 0; z < map_scheme[x][y]; z++) {
                    let cell = document.createElement("td");
                    cell.dataset.x = ""+xx;
                    cell.dataset.y = ""+i;
                    cell.dataset.power = "0";
                    cell.classList.add(base_class);
                    cell.classList.add("rop_workable_" + workable);
                    cell.classList.add("rop_tile_empty");
                    cell.addEventListener('click', function () { rop_tile_click(map, cell, map_name) });
                    row.append(cell);
                    map[xx][i] = cell;
                    i++;
                }
                workable = !workable;
            }
            xx++;
        }
    }
}
function rop_tile_click(map, cell, map_name, replaying = false) {
    initialized=true;
    if (cell.classList.contains("rop_tile_empty"))
        rop_mark_tile(map, cell, map_name, replaying);
    else if (!replaying)
        rop_drop_set(map, cell, map_name, replaying);
    rop_paint_map(map);
}
function rop_mark_tile(map, cell, map_name, replaying) {
    let cell_x = Number(cell.dataset.x);
    let cell_y = Number(cell.dataset.y);
    let north = (cell_x > 0) ? map[cell_x - 1][cell_y] : null;
    let east = (cell_y < map[0].length - 1) ? map[cell_x][cell_y + 1] : null;
    let south = (cell_x < map.length - 1) ? map[cell_x + 1][cell_y] : null;
    let west = (cell_y > 0) ? map[cell_x][cell_y - 1] : null;
    let near_water_line = rop_is_water_line(north) || rop_is_water_line(east) || rop_is_water_line(south) || rop_is_water_line(west);
    if (rop_sel_str_name === "rop_tile_pump") {
        cell.classList.add("rop_tile_pump");
        cell.dataset.power = "9";
        cell.classList.remove("rop_tile_empty");
        cell.dataset.origin_x = cell.dataset.x;
        cell.dataset.origin_y = cell.dataset.y;
        rop_href_add(cell_x, cell_y, map_name, "p", replaying);
        games_rop_status.innerText = "Placed Irrigation Pump";
        rop_select_structure("rop_tile_trench");
    }
    else if (map[cell_x][cell_y].classList.contains("rop_workable_true") && near_water_line) {
        if (rop_sel_str_name === "rop_tile_extender") {
            cell.classList.remove("rop_tile_empty");
            cell.classList.add("rop_tile_extender");
            cell.dataset.origin_x = cell.dataset.x;
            cell.dataset.origin_y = cell.dataset.y;
            cell.dataset.power = "9";
            rop_href_add(cell_x, cell_y, map_name, "e", replaying);
            games_rop_status.innerText = "Placed Irrigation Extender";
            rop_select_structure("rop_tile_trench");
        }
        else {
            let candidates = []
            if (rop_is_water_line(north))
                candidates[candidates.length] = north;
            if (rop_is_water_line(east))
                candidates[candidates.length] = east;
            if (rop_is_water_line(south))
                candidates[candidates.length] = south;
            if (rop_is_water_line(west))
                candidates[candidates.length] = west;
            for (let i = 0; i < candidates.length; i++) {
                if (map[candidates[i].dataset.origin_x][candidates[i].dataset.origin_y].dataset.power < 1) {
                    games_rop_status.innerText = "Water network out of power";
                    continue;
                }
                if (rop_tile_is_pump(map[candidates[i].dataset.x][candidates[i].dataset.y])
                    && map[candidates[i].dataset.origin_x][candidates[i].dataset.origin_y].dataset.power < 9) {
                    games_rop_status.innerText = "Pump has only one output";
                    continue;
                }
                map[candidates[i].dataset.origin_x][candidates[i].dataset.origin_y].dataset.power = ""+(map[candidates[i].dataset.origin_x][candidates[i].dataset.origin_y].dataset.power - 1);
                cell.classList.add("rop_tile_trench");
                cell.classList.remove("rop_tile_empty");
                cell.dataset.origin_x = candidates[i].dataset.origin_x;
                cell.dataset.origin_y = candidates[i].dataset.origin_y;
                cell.innerText = 9 - Number(map[candidates[i].dataset.origin_x][candidates[i].dataset.origin_y].dataset.power);
                if (Number(map[candidates[i].dataset.origin_x][candidates[i].dataset.origin_y].dataset.power) === 0)
                    rop_select_structure("rop_tile_extender");
                rop_href_add(cell_x, cell_y, map_name, "t", replaying);
                games_rop_status.innerText = "Placed Irrigation Trench";
                break;
            }
        }
    }
}
function rop_drop_set(map, target, map_name, replaying) {
    map[target.dataset.origin_x][target.dataset.origin_y].dataset.power = "9";
    if (rop_tile_is_trench(target)) {
        let selected = []
        for (let x = 0; x < map.length; x++) {
            for (let y = 0; y < map[x].length; y++) {
                if (!rop_tile_source_coords_match(map[x][y], target.dataset.origin_x, target.dataset.origin_y))
                    continue;
                if (rop_tile_is_trench(map[x][y]))
                    selected[selected.length] = map[x][y];
            }
        }
        for (let x = 0; x < selected.length; x++)
            rop_drop_tile(selected[x], map_name, replaying);
    }
    else {
        rop_drop_tile(target, map_name, replaying);
    }
}
function rop_drop_tile(target, map_name, replaying) {
    target.classList.remove("rop_tile_pump");
    target.classList.remove("rop_tile_trench");
    target.classList.remove("rop_tile_extender");
    target.dataset.power = "0";
    target.dataset.origin_x = null;
    target.dataset.origin_y = null;
    target.classList.add("rop_tile_empty");
    target.innerText = "";
    rop_href_del(target.dataset.x, target.dataset.y, map_name, replaying);
}
let rop_north_seq = [];
let rop_south_seq = [];
function rop_href_add(cell_x, cell_y, map_name, str_char, replaying) {
    let seq = map_name === "north" ? rop_north_seq : rop_south_seq;
    for (let i = 0; i < seq.length; i++)
        if (Number(seq[i].x) === Number(cell_x) && Number(seq[i].y) === Number(cell_y)) {
            console.warn("rop_href_add invalid state; attempting to add structure to already used cell", seq, str_char, cell_x, cell_y);
            return;
        }
    seq[seq.length] = { x: cell_x, y: cell_y, str: str_char };
    if (map_name === "north")
        rop_north_seq = seq;
    else
        rop_south_seq = seq;
    if (!replaying) updateOtherHrefData(map_name, map_seq_name_to_settings(map_name));
}
function rop_href_del(cell_x, cell_y, map_name, replaying) {
    let new_sequence = [];
    let old_sequence = map_name === "north" ? rop_north_seq : rop_south_seq;
    for (let i = 0; i < old_sequence.length; i++) {
        if (Number(old_sequence[i].x) === Number(cell_x) && Number(old_sequence[i].y) === Number(cell_y)) continue;
        new_sequence[new_sequence.length] = old_sequence[i];
    }
    if (map_name === "north")
        rop_north_seq = new_sequence;
    else
        rop_south_seq = new_sequence;
    if (!replaying) updateOtherHrefData(map_name, map_seq_name_to_settings(map_name));
}
function map_seq_name_to_settings(map_name) {
    return str_seq_to_href(map_name === "north" ? rop_north_seq : rop_south_seq);
}
function str_seq_to_href(str_seq) {
    let href = "";
    for (let i = 0; i < str_seq.length; i++) {
        href += str_seq[i].str + "-" + str_seq[i].x + "-" + str_seq[i].y + ";";
    }
    return href;
}
function href_to_str_seq(href) {
    let str_seq = [];
    let parts = href.split(';');
    for (let i = 0; i < parts.length; i++) {
        let components = parts[i].split('-');
        if (components.length === 0) break;
        str_seq[str_seq.length] = { str: components[0], x: Number(components[1]), y: Number(components[2]) };
    }
    return str_seq;
}
function rop_str_char_to_str_name(str_char) {
    return str_char === "p" ? "rop_tile_pump"
        : str_char === "t" ? "rop_tile_trench" : "rop_tile_extender";
}
function replay_map(map, tile_class, save, seq, map_name) {
    seq = href_to_str_seq(save);
    let cells = document.getElementsByClassName(tile_class);
    for (let i = 0; i < seq.length; i++) {
        let cell = null;
        for (let j = 0; j < cells.length; j++) {
            if (Number(cells[j].dataset.x) === Number(seq[i].x) && Number(cells[j].dataset.y) === Number(seq[i].y)) {
                cell = cells[j];
                break;
            }
        }
        if (cell === null) continue;
        rop_select_structure(rop_str_char_to_str_name(seq[i].str));
        rop_tile_click(map, cell, map_name, true);
    }
    rop_paint_map(map);
    updateOtherHrefData(map_name, map_seq_name_to_settings(map_name));
}
function rop_paint_map(map) {
    let watered = 0;
    let pumps = 0;
    let trenches = 0;
    let extenders = 0;
    for (let x = 0; x < map.length; x++) {
        for (let y = 0; y < map[x].length; y++) {
            map[x][y].classList.remove("rop_tile_watered");
            if (rop_tile_is_pump(map[x][y]))
                pumps++;
            else if (rop_tile_is_trench(map[x][y]))
                trenches++;
            else if (rop_tile_is_extender(map[x][y]))
                extenders++;
            if ((map[x][y].classList.contains("rop_workable_false") && !rop_tile_is_pump(map[x][y])) || rop_tile_is_trench(map[x][y]))
                continue;
            else if (rop_tile_is_pump(map[x][y])
                || rop_tile_is_extender(map[x][y])) {
                map[x][y].innerText = map[x][y].dataset.power;
            }
            else {
                if (x > 0 && x < map.length && y > 0 && y < map[0].length &&
                    (rop_is_water_source(map[x + 1][y])
                        || rop_is_water_source(map[x - 1][y])
                        || rop_is_water_source(map[x][y + 1])
                        || rop_is_water_source(map[x][y - 1])
                        || rop_is_water_source(map[x + 1][y + 1])
                        || rop_is_water_source(map[x + 1][y - 1])
                        || rop_is_water_source(map[x - 1][y + 1])
                        || rop_is_water_source(map[x - 1][y - 1]))) {
                    rop_tile_mark_as_watered(map[x][y]);
                    watered++;
                    continue;
                }
                try {
                    if (x > 2 && x < map.length - 1 && rop_is_water_source(map[x - 2][y]) && rop_is_water_source(map[x + 2][y])) {
                        rop_tile_mark_as_watered(map[x][y]);
                        watered++;
                        continue;
                    }
                    if (y > 2 && y < map[0].length && rop_is_water_source(map[x][y - 2]) && rop_is_water_source(map[x][y + 2])) {
                        rop_tile_mark_as_watered(map[x][y]);
                        watered++;
                    }
                } catch (Exception) {
                    console.error(Exception, x, y);
                }
            }
        }
    }
    let tin = pumps * 50;
    let copper = pumps * 50;
    let stone = pumps * 50 + trenches * 2;
    let fiber = trenches * 5;
    let yarn = extenders * 2;
    let hardwood = extenders * 25;
    document.getElementById("games_rop_stats").innerHTML = "<ul><li>Watered: " + watered + "</li><li>Pumps: " + pumps + "</li><li>Extenders: " + extenders + "</li><li>Trenches: " + trenches + "</li></ul><h2>Cost</h2><ul><li>Tin: " + tin + "</li><li>Copper: " + copper + "</li><li>Yarn: " + yarn + "</li><li>Hardwood: " + hardwood + "</li><li>Stone: " + stone + "</li><li>Fiber: " + fiber + "</li></ul>";
}
function rop_is_water_source(tile) {
    return tile && tile.dataset && (rop_tile_is_trench(tile) || rop_tile_is_extender(tile));
}
function rop_is_water_line(tile) {
    return tile && (rop_tile_is_pump(tile) || rop_tile_is_trench(tile) || rop_tile_is_extender(tile));
}
function rop_tile_source_coords_match(tile, x, y) {
    return tile.dataset.origin_x === x && tile.dataset.origin_y === y;
}
function rop_tile_is_pump(tile) {
    return tile && tile.classList && tile.classList.contains("rop_tile_pump");
}
function rop_tile_is_extender(tile) {
    return tile && tile.classList && tile.classList.contains("rop_tile_extender");
}
function rop_tile_is_trench(tile) {
    return tile && tile.classList && tile.classList.contains("rop_tile_trench");
}
function rop_tile_mark_as_watered(tile) {
    return tile && tile.classList && tile.classList.add("rop_tile_watered");
}
const Name = "Name"; const Type = "Type"; const Clan = "Clan"; const Job = "Job"; const Loves = "Loves"; const Likes = "Likes"; const Dislikes = "Dislikes"; const Hates = "Hates"; const Source = "Source"; const Usage = "Usage"; const Sources = "Sources"; const Recipe = "Recipe"; const Loved = "Loved"; const Liked = "Liked"; const Disliked = "Disliked"; const Hated = "Hated";
const person = "person"; const tool = "tool"; const item = "item";
const Pachan = "Pachan"; const Yakuan = "Yakuan"; const Mograni = "Mograni";
const PlantFiber = "Plant Fiber"; const Wood = "Wood"; const Obsidian = "Obsidian"; const Tin = "Tin"; const Copper = "Copper"; const Silver = "Silver"; const Gold = "Gold";
const Chile = "Chile"; const Pumpkin = "Pumpkin"; const Broccoli = "Broccoli"; const Quinoa = "Quinoa"; const Sesame = "Sesame"; const Teff = "Teff"; const Tomato = "Tomato"; const Lettuce = "Lettuce"; const Sunflower = "Sunflower"; const Garlic = "Garlic"; const Strawberry = "Strawberry"; const SweetPotato = "Sweet Potato"; const Kohlrabi = "Kohlrabi"; const SeaKale = "Sea Kale"; const Pineapple = "Pineapple"; const Fennel = "Fennel"; const Cabbage = "Cabbage"; const Corn = "Corn"; const Potatoes = "Potatoes"; const Eggplant = "Eggplant"; const Buckwheat = "Buckwheat"; const Onion = "Onion"; const Beets = "Beets"; const Carrot = "Carrot"; const Cassava = "Cassava"; const Amaranth = "Amaranth";
const DriedChile = "Dried Chile";
const Mango = "Mango"; const Almond = "Almond"; const Coconut = "Coconut"; const sDate = "Date"; const Pomegranate = "Pomegranate"; const Alma = "Alma"; const Avocado = "Avocado"; const Olive = "Olive";
const Oregano = "Oregano"; const Mandrake = "Mandrake"; const Mint = "Mint"; const Citronella = "Citronella"; const MilkThistle = "Milk Thistle"; const Parsley = "Parsley"; const Seaweed = "Seaweed"; const DaturaFlower = "Datura Flower"; const BurdockRoot = "Burdock Root"; const Lavender = "Lavender"; const Watercress = "Watercress"; const Alfalfa = "Alfalfa"; const Rosemary = "Rosemary"; const RoseHips = "Rose Hips"; const WillowBark = "Willow Bark";
const BrownMushroom = "Brown Mushroom"; const RedMushroom = "Red Mushroom"; const StrangeMushroom = "Strange Mushroom";
const Peridot = "Peridot"; const Sardius = "Sardius"; const Amethyst = "Amethyst"; const Nacre = "Nacre"; const Jasper = "Jasper"; const Sapphire = "Sapphire"; const Beryl = "Beryl";
const Bones = "Bones"; const OstrichEgg = "Ostrich Egg"; const Horns = "Horns"; const JunglefowlEgg = "Junglefowl Egg"; const JunglefowlFeather = "Junglefowl Feather"; const OstrichFeather = "Ostrich Feather"; const IbexMilk = "Ibex Milk"; const Honey = "Honey";
const HayBale = "Hay Bale";
const Porcupine = "Porcupine"; const Fish = "Fish";
const Hardwood = "Hardwood"; const Flint = "Flint";
const Oil = "Oil"; const Flour = "Flour"; const Grain = "Grain";
function onlyWild(name) {
    return name + " (only wild)";
}
function onlyDomesticated(name) {
    return name + " (only domesticated)";
}
function allKinds(name) {
    return name + " (all)";
}
function dried(name) {
    return "Dried " + name;
}
function smoked(name) {
    return "Smoked " + name;
}
function pickled(name) {
    return "Pickled " + name;
}
function juice(name) {
    return name + " Juice";
}
let games_rop_database = [
    { Name: "Era", Type: person, Clan: Pachan, Job: "", Loves: [Chile, Mango, Oregano, Peridot, Pumpkin, Silver], Likes: [Almond, Broccoli, DriedChile, Quinoa, Sesame, Teff, Tomato], Dislikes: [Bones, BrownMushroom, Lettuce, Mandrake, RedMushroom, StrangeMushroom, Wood], Hates: [Citronella, HayBale, Parsley, PlantFiber, Seaweed, Strawberry, SweetPotato] },
    { Name: "Frer", Type: person, Clan: Pachan, Job: "", Loves: [Kohlrabi, Mango, Sardius], Likes: [Porcupine, SeaKale] },
    { Name: "Garrek", Type: person, Clan: Pachan, Job: "", Loves: [Mango, Seaweed], Likes: [BrownMushroom, MilkThistle, Sunflower] },
    { Name: "Ibon", Type: person, Clan: Pachan, Job: "", Loves: [Amethyst, DaturaFlower, Kohlrabi, Mango, Pineapple, Sunflower], Likes: [BurdockRoot, Coconut, sDate, Lavender, OstrichEgg, Silver] },
    { Name: "Illoe", Type: person, Clan: Pachan, Job: "", Loves: [BurdockRoot, Gold, Mango, Nacre, Sunflower, Watercress], Likes: [Citronella, DaturaFlower, Lavender, MilkThistle, Mint, OstrichEgg, Sesame] },
    { Name: "Jelrod", Type: person, Clan: Pachan, Job: "", Loves: [Gold, Jasper, Mango], Likes: [Alfalfa, Coconut, Quinoa, Sesame, Sunflower] },
    { Name: "Jukk", Type: person, Clan: Pachan, Job: "", Loves: [Bones, Broccoli, Chile, Gold, Mango, Nacre, Sapphire], Likes: [Coconut, sDate, Fennel, Horns, Pomegranate, Pumpkin, Silver] },
    { Name: "Mana", Type: person, Clan: Pachan, Job: "", Loves: [Mango, Quinoa], Likes: [Almond, Broccoli, Garlic, Horns, Pineapple] },
    { Name: "Krak", Type: person, Clan: Yakuan, Job: "", Loves: [Cabbage, Horns, Sapphire, Sunflower], Likes: [Copper, Corn, Hardwood, Mango, Mint, Pumpkin, Strawberry] },
    { Name: "Touk", Type: person, Clan: Mograni, Job: "", Loves: [Alfalfa, DaturaFlower, Mango, Peridot, Pineapple, Potatoes], Likes: [BurdockRoot, Garlic, Pomegranate, Sunflower] },
    { Name: "Vallah", Type: person, Clan: Mograni, Job: "", Loves: [Amethyst, Copper, Kohlrabi, Mango, Mint, Oregano, Pumpkin, Rosemary], Likes: [Alma, BrownMushroom, Coconut, Flint, Hardwood, Obsidian] },
    { Name: "Acre", Type: person, Clan: Pachan, Job: "", Loves: [Avocado, Copper, Mango, Obsidian, Tin], Likes: [Corn, Flint] },
    { Name: "Ada", Type: person, Clan: Pachan, Job: "Medicinal Ingredients trader", Loves: [BurdockRoot, Eggplant, Mandrake, Mango, RedMushroom, RoseHips, Seaweed, WillowBark], Likes: [Buckwheat, Citronella, DaturaFlower, Lavender, MilkThistle, Mint, Rosemary] },
    { Name: "Ata", Type: person, Clan: Pachan, Job: "", Loves: [Copper, JunglefowlFeather, Obsidian, Strawberry], Likes: [Flint, Hardwood, Kohlrabi, Mango, Pineapple, Pumpkin, Sunflower] },
    { Name: "Brah", Type: person, Clan: Pachan, Job: "", Loves: [Mango, Peridot, Potatoes, Strawberry], Likes: [Coconut, Nacre, OstrichEgg, Pomegranate] },
    { Name: "Croll", Type: person, Clan: Pachan, Job: "Builder", Loves: [Mango], Likes: [Bones, Horns, Onion] },
    { Name: "Daari", Type: person, Clan: Pachan, Job: "", Loves: [onlyDomesticated(Amaranth), Mango, MilkThistle, Oregano, OstrichFeather], Likes: [allKinds(Flour), allKinds(Oil), OstrichEgg] },
    { Name: "Gin", Type: person, Clan: Pachan, Job: "", Loves: [Amethyst, Corn, JunglefowlEgg, Mango, OstrichEgg, Potatoes], Likes: [Chile, Horns, IbexMilk, OstrichEgg, WillowBark] },
    { Name: "Grob", Type: person, Clan: Pachan, Job: "Hunter", Loves: [Beets, Mango, Potatoes], Likes: [Corn, Flint, Garlic, Hardwood, Horns, Onion, Peridot] },
    { Name: "Igrork", Type: person, Clan: Pachan, Job: "Seed trader", Loves: [Almond, Beryl, Mango, Teff], Likes: [Beets, Cabbage, Carrot, Cassava, sDate, Eggplant, Garlic, Onion, Potatoes, SweetPotato] },
    { Name: "Jag", Type: person, Clan: Pachan, Job: "", Loves: [Hardwood, allKinds(Honey), Lavender, pickled(Carrot), Sesame], Likes: [Flint, Mango, onlyWild(juice(Mango)), Pomegranate, onlyDomesticated(juice(Pomegranate)), smoked(allKinds(Fish)), Wood] },
    { Name: "Jizu", Type: person, Clan: Pachan, Job: "", Loves: [Citronella, Fennel, Mango, Sardius], Likes: [Bones, JunglefowlFeather, onlyWild(juice(Mango)), MilkThistle, Nacre, Oregano, OstrichFeather, onlyDomesticated(juice(Pomegranate))] },
    { Name: "Maeri", Type: person, Clan: Pachan, Job: "", Loves: [Amethyst, Mango, Olive, pickled(Beets), Sunflower, WillowBark], Likes: [dried(Chile), allKinds(Flour), onlyWild(juice(Mango)), OstrichFeather, pickled(Eggplant), onlyDomesticated(juice(Pomegranate))] },
    { Name: "Nari", Type: person, Clan: Pachan, Job: "", Loves: [Amaranth, Beryl, Cabbage, Copper, dried(Chile), Mango, pickled(Chile)], Likes: [Chile, DaturaFlower, onlyWild(juice(Mango)), Nacre, Pomegranate, onlyDomesticated(juice(Pomegranate)), Teff] },
    { Name: "Nokk", Type: person, Clan: Pachan, Job: "", Loves: [Carrot, Mango, Oregano, Peridot, Rosemary, Tomato], Likes: [BrownMushroom, Corn, Eggplant, Parsley, Sesame] },
    { Name: "Okka", Type: person, Clan: Pachan, Job: "", Loves: [Avocado, Citronella, Mango, OstrichEgg, Sapphire], Likes: [Pumpkin, Sunflower, Tomato] },
    { Name: "Reese", Type: person, Clan: Pachan, Job: "", Loves: [Lavender, Mango, Nacre, OstrichFeather, StrangeMushroom, Sunflower], Likes: [Almond, Beets, Broccoli, MilkThistle, Pomegranate] },
    { Name: "Ron", Type: person, Clan: Pachan, Job: "", Loves: [allKinds(Grain)], Likes: [Mango, Pomegranate, Strawberry] },
    { Name: "Tare", Type: person, Clan: Pachan, Job: "Cooking tools trader", Loves: [Beryl, Chile, Mango, Oregano, Parsley], Likes: [BrownMushroom, Garlic, Quinoa, Sunflower] },
    { Name: "Tetih", Type: person, Clan: Pachan, Job: "", Loves: [Amethyst, Cassava, Coconut, Gold, Mango, Sapphire], Likes: [sDate, MilkThistle, Peridot, Sardius, StrangeMushroom, WillowBark] },
    { Name: "Voda", Type: person, Clan: Pachan, Job: "", Loves: [Buckwheat, Lavender, Mango], Likes: [Beets, Chile, Eggplant, Strawberry] },
    { Name: "Vor", Type: person, Clan: Pachan, Job: "", Loves: [Mango, juice(Strawberry)], Likes: [Corn, OstrichEgg, Pomegranate, Potatoes, Strawberry, Sunflower] },
    { Name: "Vuak", Type: person, Clan: Pachan, Job: "Shaman", Loves: [Mandrake, Mango, Pumpkin], Likes: [Beets, Bones, BurdockRoot, Citronella, DaturaFlower, Horns, Lavender, MilkThistle, Mint, Obsidian, RoseHips, Rosemary, StrangeMushroom, WillowBark] },
    { Name: "Zelk", Type: person, Clan: Pachan, Job: "Trader", Loves: [BrownMushroom, Eggplant, Mango, RedMushroom, StrangeMushroom], Likes: [sDate, Flint, Mandrake, OstrichEgg, Pomegranate, Tomato] },
    { Name: "Ash", Type: person, Clan: Yakuan, Job: "", Loves: [], Likes: [] },
    { Name: "Brub", Type: person, Clan: Yakuan, Job: "", Loves: [Mango, pickled(Porcupine)], Likes: [Amethyst, Nacre] },
    { Name: "Inza", Type: person, Clan: Yakuan, Job: "", Loves: [Broccoli, Mango, Mint, Peridot, Strawberry], Likes: [Nacre, Pomegranate] },
    { Name: "Javvi", Type: person, Clan: Yakuan, Job: "", Loves: [], Likes: [] },
    { Name: "Kocha", Type: person, Clan: Yakuan, Job: "", Loves: [], Likes: [] },
    { Name: "Pellek", Type: person, Clan: Yakuan, Job: "", Loves: [], Likes: [] },
    { Name: "Anda", Type: person, Clan: Mograni, Job: "", Loves: [], Likes: [] },
    { Name: "Cohh", Type: person, Clan: Mograni, Job: "", Loves: [], Likes: [] },
    { Name: "Havri", Type: person, Clan: Mograni, Job: "", Loves: [], Likes: [] },
    { Name: "Zeda", Type: person, Clan: Mograni, Job: "", Loves: [Amethyst, Chile, Garlic, Gold, Kohlrabi, Mango, Mint, Silver], Likes: [Broccoli, Eggplant, OstrichEgg, pickled(Eggplant), Sesame] },
    { Name: "Water Bucket", Type: tool, Usage: "watering crops", Source: "Acre", Recipe: { Hardwood: 20 } },
    { Name: "Copper Hoe", Type: tool, Usage: "tiling", Source: "Smith", Recipe: {} },
    { Name: "Copper Sickle", Type: tool, Usage: "cutting plants", Source: "Smith", Recipe: {} },
    { Name: "Copper Watering Can", Type: tool, Usage: "watering crops", Source: "Smith", Recipe: {} },
    { Name: "Bronze Axe", Type: tool, Usage: "chopping wood", Source: "Smith", Recipe: { Tin: 15, Copper: 20, Hardwood: 15 } },
    { Name: "Bronze Hammer", Type: tool, Usage: "rocks and ore nodes", Source: "Smith", Recipe: { Tin: 20, Copper: 20, Hardwood: 15 } },
    { Name: "Bronze Hoe", Type: tool, Usage: "tiling", Source: "Smith", Recipe: { Tin: 15, Copper: 20, Hardwood: 15 } },
    { Name: "Bronze Sickle", Type: tool, Usage: "cutting crops", Source: "Smith", Recipe: { Tin: 15, Copper: 20, Hardwood: 30 } },
    { Name: "Bronze Watering Can", Type: tool, Usage: "watering crops", Source: "Smith", Recipe: { Tin: 20, Copper: 25, Hardwood: "?" } },
    { Name: DaturaFlower, Type: item, Sources: ["Gathering"], Loved: ["Ibon", "Touk"], Liked: ["Ada", "Illoe", "Nari", "Vuak"], Disliked: [], Hated: [] },
];
let games_rop_data_query = document.getElementById("games_rop_data_query");
let games_rop_data = document.getElementById("games_rop_data");
games_rop_data_query.addEventListener('keyup', function () { games_rop_data_search(); });
function games_rop_data_search() {
    let query = games_rop_data_query.value;
    let output = "";
    if (query.length > 2) {// no search on super wide searches to avoid element add spike
        let matches = []
        games_rop_database.forEach(value => {
            if (query.localeCompare(value.Name, 'en', { sensitivity: 'base' }) === 0)
                matches[matches.length] = value;
        });
        games_rop_database.forEach(value => {
            if (query.localeCompare(value.Name, 'en', { sensitivity: 'base' }) !== 0 && value.Name.search(new RegExp(query, "i")) !== -1)
                matches[matches.length] = value;
        });
        output += "<ul>";
        for (let i = 0; i < matches.length; i++) {
            try {
                switch (matches[i].Type) {
                    case person:
                        output += "<li><table><tr><th>Person</th><th>Loves</th><th>Likes</th><th>Dislikes</th><th>Hates</th></tr><tr><td><h3>" + matches[i].Name + "</h3><p>" + matches[i].Clan + " clan</p>";
                        if (matches[i].Job !== "")
                            output += "<hr><h4>Job</h4><p>" + matches[i].Job + "</p>";
                        output += "</td><td><ul>";
                        if (matches[i].Loves != null)
                            matches[i].Loves.forEach(value => { output += "<li>" + value + "</li>"; });
                        output += "</ul></td><td><ul>";
                        if (matches[i].Likes != null)
                            matches[i].Likes.forEach(value => { output += "<li>" + value + "</li>"; });
                        output += "</ul></td><td><ul>";
                        if (matches[i].Dislikes != null)
                            matches[i].Dislikes.forEach(value => { output += "<li>" + value + "</li>"; });
                        output += "</ul></td><td><ul>";
                        if (matches[i].Hates != null)
                            matches[i].Hates.forEach(value => { output += "<li>" + value + "</li>"; });
                        output += "</ul></td></tr></table></li>";
                        break;
                    case item:
                        output += "<li><table><tr><th>Item</th><th>Loved</th><th>Liked</th><th>Disliked</th><th>Hated</th></tr><tr><td><table><tr><td>" + matches[i].Name + "</td></tr><tr><td>Sources</td></tr><tr><td><ul>";
                        matches[i].Sources.forEach(value => { output += "<li>" + value + "</li>"; });
                        output += "</ul></td></tr></table></td><td><ul>";
                        if (matches[i].Loved != null)
                            matches[i].Loved.forEach(value => { output += "<li>" + value + "</li>"; });
                        output += "</ul></td><td><ul>";
                        if (matches[i].Liked != null)
                            matches[i].Liked.forEach(value => { output += "<li>" + value + "</li>"; });
                        output += "</ul></td><td><ul>";
                        if (matches[i].Disliked != null)
                            matches[i].Disliked.forEach(value => { output += "<li>" + value + "</li>"; });
                        output += "</ul></td><td><ul>";
                        if (matches[i].Hated != null)
                            matches[i].Hated.forEach(value => { output += "<li>" + value + "</li>"; });
                        output += "</ul></td></tr></table></li>";
                        break;
                    case tool:
                        output += "<li><table><tr><th>Tool</th><th>Usage</th><th>Source</th><th>Recipe</th></tr><tr><td>" + matches[i].Name + "</td><td>" + matches[i].Usage + "</td><td>" + matches[i].Source + "</td><td><ul>";
                        Object.entries(matches[i].Recipe).forEach(([key, value]) => {
                            output += "<li><b>" + key + "</b>: " + value + "</li>";
                        });
                        output += "</ul></td></table></li>";
                        break;
                    default:
                        output += "<li><table><tr><th>Other</th></tr><tr><td>" + matches[i].Name + "</td></tr></table></li>";
                        break;
                }
                if (i < matches.length - 1)
                    output += "<li><hr></li>"
            } catch (Exception) {
                console.error(Exception);
            }
        }
        output += "</ul>";
    }
    games_rop_data.innerHTML = output;
}