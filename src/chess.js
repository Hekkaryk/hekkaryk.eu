let seq_hor = " abcdefgh ";
let seq_ver = " 87654321 ";
let def_seq = "a8br,b8bn,c8bb,d8bq,e8bk,f8bb,g8bn,h8br,a7bp,b7bp,c7bp,d7bp,e7bp,f7bp,g7bp,h7bp,a2wp,b2wp,c2wp,d2wp,e2wp,f2wp,g2wp,h2wp,a1wr,b1wn,c1wb,d1wq,e1wk,f1wb,g1wn,h1wr";
let dict = {
    br: "&#9814;",
    bn: "&#9816;",
    bb: "&#9815;",
    bq: "&#9813;",
    bk: "&#9812;",
    bp: "&#9817;",
    wr: "&#9820;",
    wn: "&#9822;",
    wb: "&#9821;",
    wq: "&#9819;",
    wk: "&#9818;",
    wp: "&#9823;"
};
let chess_board_div = document.getElementById("chess_board_div");
let boardElement = document.createElement("table");
chess_board_div.append(boardElement);
let even_ver = false;
let even_hor = false;
let board = [];
function init() {
    for (let y = 0; y < seq_ver.length; y++) {
        board[y] = [];
        let row = document.createElement("tr");
        for (let x = 0; x < seq_hor.length; x++) {
            let cell = document.createElement("td");
            cell.dataset.x = x;
            cell.dataset.y = y;
            cell.classList.add("chessboard", "chess_tile");
            if (x > 0 && x < seq_hor.length - 1 && y > 0 && y < seq_ver.length - 1)
                cell.classList.add("board_" + ((even_hor == true && even_ver == true) || (even_hor == false && even_ver == false)
                    ? "white" : "black"));
            if (y == 0 || y == seq_ver.length - 1)
                cell.innerHTML = seq_hor[x];
            if (x == 0 || x == seq_hor.length - 1)
                cell.innerHTML = seq_ver[y];
            cell.addEventListener('click', function () { chess_tile_click(board, cell) });
            row.append(cell);
            board[y][x] = cell;
            even_hor = !even_hor;
        }
        boardElement.append(row);
        even_ver = !even_ver;
        even_hor = false;
    }
}
let selected = null;
function chess_tile_click(board, cell) {
    if (selected == null) {
        selected = cell;
        return;
    }
    let ih = board[selected.dataset.y][selected.dataset.x].innerHTML;
    let code=board[selected.dataset.y][selected.dataset.x].dataset.code;
    board[selected.dataset.y][selected.dataset.x].innerHTML = "";
    board[selected.dataset.y][selected.dataset.x].dataset.code = "";
    board[cell.dataset.y][cell.dataset.x].innerHTML = ih;
    board[cell.dataset.y][cell.dataset.x].dataset.code = code;
    selected = null;
    repaint();
}
function repaint(updateHref = true) {
    let elem = document.getElementsByClassName("chess_tile");
    let str="";
    for (let i = 0; i < elem.length; i++) {
        let dat = board[elem[i].dataset.y][elem[i].dataset.x];
        elem[i].innerHTML = dat.innerHTML;
        elem[i].dataset.code=dat.dataset.code;
        let text="";
        for(let key in dict){
            if (key === dat.dataset.code){
                text=key;
                break;
            }
        }
        if (text !== ""){
            if (str !== "") str+=",";
            str+=""+seq_hor[elem[i].dataset.x]+""+seq_ver[elem[i].dataset.y]+""+text;
        }
    }
    if (updateHref){
        updateOtherHrefData("chessboard", str);
    }
}

let wlh = window.location.href;
init();
if (wlh.indexOf("chessboard") !== -1) {
    let str = wlh.substring(wlh.indexOf("chessboard") + "chessboard".length);
    str = str.split('=');
    str = str[1].split(';');
    replay(str_to_seq(str[0]));
}
else {
    replay(str_to_seq(def_seq));
}
function str_to_seq(str) {
    return str.split(',');
}
function replay(seq) {
    for (let y = 1; y < seq_ver.length - 1; y++)
        for (let x = 1; x < seq_hor.length - 1; x++)
            board[y][x].innerHTML = "";
    for (let i = 0; i < seq.length; i++) {
        let x = seq_hor.indexOf(seq[i][0]);
        let y = seq_ver.indexOf(seq[i][1]);
        let text = "";
        let code = "" + seq[i][2] + "" + seq[i][3];
        for(let key in dict){
            if (key === code){
                text=dict[key];
                break;
            }
        }
        board[y][x].innerHTML = text;
        board[y][x].dataset.code=code;
    }
    repaint(false);
}