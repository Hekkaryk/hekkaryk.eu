
let tool_encoder_input = document.getElementById("tool_encoder_input");
let tool_encoder_output = document.getElementById("tool_encoder_output");
document.getElementById("tool_base64_encode").addEventListener('click', function () { base64encode(); });
document.getElementById("tool_base64_decode").addEventListener('click', function () { base64decode(); });
document.getElementById("tool_base64_encode_unicode").addEventListener('click', function () { base64encodeUnicode(); });
document.getElementById("tool_base64_decode_unicode").addEventListener('click', function () { base64decodeUnicode(); });
document.getElementById("tool_encode_URI_component").addEventListener('click', function () { hk_encodeURIComponent(); });
document.getElementById("tool_decode_URI_component").addEventListener('click', function () { hk_decodeURIComponent(); });
document.getElementById("tool_encode_HTML").addEventListener('click', function () { encodeHTML(); });
document.getElementById("tool_decode_HTML").addEventListener('click', function () { decodeHTML(); });
document.getElementById("tool_encode_image_to_html").addEventListener('click', function () { encodeImageToHTML(); });
function base64encode() {
    tool_encoder_output.value = window.btoa((tool_encoder_input.value));
}
function base64decode() {
    tool_encoder_output.value = window.atob(tool_encoder_input.value);
}
function base64encodeUnicode() {
    tool_encoder_output.value = btoa(encodeURIComponent(tool_encoder_input.value));
}
function base64decodeUnicode() {
    tool_encoder_output.value = decodeURIComponent(atob(tool_encoder_input.value));
}
function hk_encodeURIComponent() {
    tool_encoder_output.value = encodeURIComponent(tool_encoder_input.value);
}
function hk_decodeURIComponent() {
    tool_encoder_output.value = decodeURIComponent(tool_encoder_input.value);
}
function encodeHTML() {
    tool_encoder_output.value = encode_to_html(tool_encoder_input.value);
}
function decodeHTML() {
    let foo = document.createElement('textarea');
    foo.innerHTML = tool_encoder_input.value;
    let bar = foo.value;
    bar = reallyReplace(bar, " ", " ");
    tool_encoder_output.value = bar;
}
function encodeImageToHTML() {
    const file = document.getElementById("tool_encode_image_to_html").files[0];
    const reader = new FileReader();
    reader.onloadend = function () {
        tool_encoder_output.value = "<img src=\"" + reader.result + "\" alt='' />";
    };
    reader.readAsDataURL(file);
}