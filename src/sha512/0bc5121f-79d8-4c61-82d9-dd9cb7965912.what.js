let basic_web_colors = {
    "white": "#FFFFFF",
    "silver": "#C0C0C0",
    "gray": "#808080",
    "black": "#000000",
    "red": "#FF0000",
    "maroon": "#800000",
    "yellow": "#FFFF00",
    "olive": "#808000",
    "lime": "#00FF00",
    "green": "#008000",
    "aqua": "#00FFFF",
    "teal": "#008080",
    "blue": "#0000FF",
    "navy": "#000080",
    "fuchsia": "#FF00FF",
    "purple": "#800080",
};
let colors = {
    "green": "#00FF00",
    "cyan": "#00FFFF",
    "pink": "#FF00FF",
    "brown": "#964B00",
    "bronze": "#CD7F32",
    "cinnabar": "#E34234",
    "copper": "#B87333",
    "coral": "#FF7F50",
    "crimson": "#DC143C",
    "ebony": "#555D50",
    "emerald": "#50C878",
    "darkgray": "#A9A9A9",
    "lightgray": "#D3D3D3",
    "pigment red": "#ED1B24",
    "madder": "#A50021",
    "cardinal red": "#C51E3A",
    "apple red": "#BE0032",
    "poppy red": "#DC343B",
    "carmine": "#960018",
    "torch red": "#E60026",
    "red cosmos": "#58111A",
    "rosewood": "#65000B",
    "imperial red": "#ED2939",
    "cordovan": "#893F45",
    "fire engine red": "#CE2029",
    "rose vale": "#AB4E52",
    "old rose": "#C08081",
    "light red": "#FF7F7F",
    "light coral": "#F08080",
    "garnet": "#733635",
    "rose ebony": "#674846",
    "chili red": "#E03C31",
    "vermilion": "#E34234",
    "misty rose": "#FFE4E1",
    "salmon": "#FA8072",
    "coral pink": "#F88379",
    "barn red": "#7C0902",
    "blood red": "#660000",
    "tea rose": "#F4C2C2",
    "dark red": "#8B0000",
    "fire brick": "#B22222",
    "redwood": "#A45953",
    "scarlet": "#FF2400",
    "tomato": "#FF6347",
    "jasper": "#D05340",
    "amaranth": "#E52B50",
    "fluorescent red": "#FF2226",
    "office green": "#008000",
    "dark green": "#006400",
    "light green": "#90EE90",
    "lime green": "#32CD32",
    "bright green": "#66FF00",
    "pale green": "#98FB98",
    "erin": "#00FF40",
    "neon green": "#39FF14",
    "apple green": "#8AB800",
    "evergreen": "#05472A",
    "forest green": "#228B22",
    "jungle green:": "#29AB87",
    "kombu green": "#354230",
    "moss green": "#8A9A5B",
    "mint green": "#98FB98",
    "myrtle": "#21421E",
    "jade": "#00A86B",
    "malachite": "#0BDA51",
    "periwinkle": "#CCCCFF",
    "ultramarine": "#120A8F",
    "medium blue": "#0000CD",
    "neon blue": "#4D4DFF",
    "dark blue": "#00008B",
    "midnight blue": "#191970",
    "light blue": "#ADD8E6",
    "sapphire": "#082567",
    "fluorescent blue": "#15F4EE",
    "azure": "#0080FF",

}
let ascii = {
    32: ' ',
    33: '!',
    34: '"',
    35: '#',
    36: '$',
    37: '%',
    38: '&',
    39: '\'',
    40: '(',
    41: ')',
    42: '*',
    43: '+',
    44: ',',
    45: '-',
    46: '.',
    47: '/',
    48: '0',
    49: '1',
    50: '2',
    51: '3',
    52: '4',
    53: '5',
    54: '6',
    55: '7',
    56: '8',
    57: '9',
    58: ':',
    59: ';',
    60: '<',
    61: '=',
    62: '>',
    63: '?',
    64: '@',
    65: 'A',
    66: 'B',
    67: 'C',
    68: 'D',
    69: 'E',
    70: 'F',
    71: 'G',
    72: 'H',
    73: 'I',
    74: 'J',
    75: 'K',
    76: 'L',
    77: 'M',
    78: 'N',
    79: 'O',
    80: 'P',
    81: 'Q',
    82: 'R',
    83: 'S',
    84: 'T',
    85: 'U',
    86: 'V',
    87: 'W',
    88: 'X',
    89: 'Y',
    90: 'Z',
    91: '[',
    92: '\\',
    93: ']',
    94: '^',
    95: '_',
    96: '`',
    97: 'a',
    98: 'b',
    99: 'c',
    100: 'd',
    101: 'e',
    102: 'f',
    103: 'g',
    104: 'h',
    105: 'i',
    106: 'j',
    107: 'k',
    108: 'l',
    109: 'm',
    110: 'n',
    111: 'o',
    112: 'p',
    113: 'q',
    114: 'r',
    115: 's',
    116: 't',
    117: 'u',
    118: 'v',
    119: 'w',
    120: 'x',
    121: 'y',
    122: 'z',
    123: '{',
    124: '|',
    125: '}',
    126: '~'
}
let symbols = {
    '0': "zero",
    '1': "one",
    '2': "two",
    '3': "three",
    '4': "four",
    '5': "five",
    '6': "six",
    '7': "seven",
    '8': "eight",
    '9': "nine",
    'a': "Alfa",
    'b': "Bravo",
    'c': "Charlie",
    'd': "Delta",
    'e': "Echo",
    'f': "Foxtrot",
    'g': "Golf",
    'h': "Hotel",
    'i': "India",
    'j': "Juliett",
    'k': "Kilo",
    'l': "Lima",
    'm': "Mike",
    'n': "November",
    'o': "Oscar",
    'p': "Papa",
    'q': "Quebec",
    'r': "Romeo",
    's': "Sierra",
    't': "Tango",
    'u': "Uniform",
    'v': "Victor",
    'w': "Whiskey",
    'x': "X-ray",
    'y': "Yankee",
    'z': "Zulu",
};
let symbols2 = {
    '`': "backtick",
    '~': "tilda",
    '!': "exclamation",
    '@': "at",
    '#': "hash",
    '$': "dollar",
    '%': "percent",
    '^': "caret",
    '&': "ampersand",
    '*': "asterisk",
    '(': "opening parenthesis",
    ')': "closing parenthesis",
    '-': "minus",
    '_': "underscore",
    '=': "equal",
    '+': "plus",
    '[': "opening square brackets",
    '{': "opening curly brackets",
    ']': "closing square brackets",
    '}': "closing curly brackets",
    '\\': "backslash",
    '|': "pipe (vertical bar)",
    ';': "semicolon",
    ':': "colon",
    '"': "quotation",
    ',': "comma",
    '<': "opening angle brackets",
    '.': "dot",
    '>': "closing angle brackets",
    '/': "slash",
    '?': "question",
    ' ': "space"
};
let input = document.getElementById("tool_what_input");
let output = document.getElementById("tool_what_output");
let s_chars = "`1234567890-=qwertyuiop[]\\asdfghjkl;'zxcvbnm,./~!@#$%^&*()_+QWERTYUIOP{}|ASDFGHJKL:\"ZXCVBNM<>?";
input.addEventListener("keyup", analyze);
let hint = document.getElementById("tool_what_hint");

function analyze(init = false) {
    if (init && input.value === "" && window.location.href.indexOf("?") !== -1){
        let index = window.location.href.indexOf("v=");
        if (index !== -1){
            input.value = window.location.href.substring(index+2);
            setBottomText("Input value loaded from URI: " + input.value);
        }
    }
    let looksLikeMorse = looks_like_morse(input.value);
    if (input.value.length === 0) {
        output.innerHTML="";
        hint.classList.remove("hidden");
        return;
    }
    hint.classList.add("hidden");
    if (uuid_check(input.value))
        return;
    if (password_check(input.value))
        return;
    if (looksLikeMorse) {
        set_output(decode_morse(input.value) + transcribe(input.value));
        return;
    } else {
        document.getElementById("morse_prosigns").classList.add("hidden");
    }
    if (equation(input.value))
        return;
    set_output(visualize_color(input.value)
        + check_string_stats(input.value)
        + transcribe(input.value)
        + decimal_to_binary(input.value)
        + binary_to_decimal(input.value)
        + parse_as_size(input.value)
        + get_factors(input.value)
        + (looksLikeMorse ? encode_morse(input.value) : "")
        + string_to_ascii(input.value)
        + ascii_to_string(input.value));
}

function set_output_direct(foo) {
    output.innerHTML = foo;
    make_clipboard_able("clipboard_able_special");
}

function set_output(foo, bar, extra) {
    set_output_direct("<p>" + foo + "<ul>" + bar + "</ul></p>" + extra);
}

function uuid_check(foo) {
    if (!same_except_case(foo, "uuid") && !same_except_case(foo, "guid"))
        return false;
    set_output("UUID examples:", generate_uuids(10).join(''), transcribe(input));
    return true;
}

function password_check(foo) {
    if (!same_except_case(foo, "password"))
        return false;
    set_output("Example passwords:", generate_passwords(16, 10).join(''), transcribe(input));
    return true;
}

function generate_uuids(count, raw) {
    let out = [];
    for (let i = 0; i < count; i++) {
        out[i] = crypto.randomUUID();
    }
    if (raw) return out;
    for (let i = 0; i < count; i++) {
        out[i] = "<li><code class=\"clipboard_able_special\" lang=\"zxx\">" + (out[i]) + "</code></li>";
    }
    return out;
}

function generate_passwords(length, count, raw) {
    let out = [];
    for (let i = 0; i < count; i++) {
        out[i] = [];
        for (let j = 0; j < length; j++)
            out[i][j] = s_chars[Math.floor(Math.random() * (s_chars.length + 1))];
    }
    if (raw) return out;
    for (let i = 0; i < count; i++) {
        out[i] = "<li><code class=\"clipboard_able_special\" lang=\"zxx\">" + encode_to_html(out[i].join('')) + "</code></li>";
    }
    return out;
}

const cth = "complexity too high"
const recusionLimitDefault=1000;
let recursionLimit = recusionLimitDefault;
const charsInEquations = ['+', '-', '*', '/', '^', '(', ')'];
const charsOperations = ['+', '-', '*', '/','^'];
function equation(input) {
    recursionLimit=recusionLimitDefault;
    let itsEquation = charsInEquations.some(x => input.includes(x));
    if (itsEquation) {
        if (charsOperations.some(x=>input[input.length-1]===x)){
            set_output_direct("Waiting for input (equation ends with operation)" + transcribe(input));
            return true;
        }
        if ((input.match(/\(/) || []).length !== (input.match(/\)/) || []).length){
            set_output_direct("Waiting for input (number of opening and closing parenthesis is different)" + transcribe(input));
            return true;
        }
        let foo = input;
        if (input[0] === '=')
            foo = foo.substring(1);
        let out = compute_outer(foo);
        if (out === cth)
            set_output_direct("Complexity too high" + transcribe(input));
        else
            set_output("Result:", "<code class=\"clipboard_able_special\" lang=\"zxx\">" + out + "</code>", transcribe(input));
    }
    return itsEquation;
}

function compute_outer(input) {
    if (recursionLimit < 0) return cth;
    recursionLimit -= 1;
    let index = input.lastIndexOf('(');
    if (index !== -1) {
        let str = input.substring(index + 1);
        let indexEnd = str.indexOf(')');
        str = str.substring(0, indexEnd);
        return compute_outer(input.substring(0, index) + compute_intermediate(str) + input.substring(index + indexEnd + 2));
    }
    return compute_intermediate(input);
}

const powerRegExp = /[0-9]+\.*[0-9]*\^-*[0-9]+\.*[0-9]*/;
const multiplicationRegExp = /[0-9]+\.*[0-9]*\*-*[0-9]+\.*[0-9]*/;
const divisionRegExp = /[0-9]+\.*[0-9]*\/-*[0-9]+\.*[0-9]*/;
const subtractionRegExp = /[0-9]+\.*[0-9]*--*[0-9]+\.*[0-9]*/;
const additionRegExp = /[0-9]+\.*[0-9]*\+-*[0-9]+\.*[0-9]*/;
function compute_intermediate(input) {
    recursionLimit -= 1;
    if (recursionLimit < 0) return cth;
    let out = input;
    let operation = '';
    let match = '';
    let index = 0;
    let power = input.match(powerRegExp);
    let indexOfPower = power === null ? -1 : input.indexOf(power[0]);
    if (power){
        operation='^';
        index=indexOfPower;
        match=power[0];
    } else {
        let multiplication = input.match(multiplicationRegExp);
        let division = input.match(divisionRegExp);
        let indexOfMultiplication =multiplication===null ? -1 : input.indexOf(multiplication[0]);
        let indexOfDivision =division===null ? -1 : input.indexOf(division[0]);
        if (multiplication && (division===null || indexOfMultiplication < indexOfDivision)) {
            operation='*';
            index=indexOfMultiplication;
            match=multiplication[0];
        } else if (division) {
            operation='/';
            index=indexOfDivision;
            match=division[0];
        } else {
            let addition = input.match(additionRegExp);
            let subtraction = input.match(subtractionRegExp);
            let indexOfAddition =addition===null ? -1 : input.indexOf(addition[0]);
            let indexOfSubtraction =subtraction===null ? -1 : input.indexOf(subtraction[0]);
            if (addition && (subtraction === null || indexOfAddition < indexOfSubtraction)) {
                operation='+';
                index=indexOfAddition;
                match=addition[0];
            } else if (subtraction) {
                operation='-';
                index=indexOfSubtraction;
                match=subtraction[0];
            }
        }
    }
    if (operation !== ''){
        let left = input.substring(0,index);
        let middle = input.substring(index,index + match.length);
        let right = input.substring(index + match.length);
        let parts = middle.split(operation);
        out = compute_intermediate(left+compute_result(parts[0],parts[1],operation)+right);
    }
    return Number(out);
}
function compute_result(first,second,operation) {
    if (operation === '^')
        return Math.pow(Number(first), Number(second));
    let index_first = first.indexOf('.');
    let index_second = second.indexOf('.');
    if (index_first === -1 && index_second === -1) {
        switch (operation) {
            case '*':
                return Number(first) * Number(second);
            case '/':
                return Number(first) / Number(second);
            case '+':
                return Number(first) + Number(second);
            case '-':
                return Number(first) - Number(second);
        }
    }
    let scope_first = first.length-index_first-1;
    let scope_second = second.length-index_second-1;
    let scope = Math.max(scope_first,scope_second);
    let out = 0;
    switch (operation) {
        case '*':
            out = Number(warpUp(first,scope)) * Number(warpUp(second,scope));
            scope=scope*2;
            break;
        case '/':
            return Number(warpUp(first,scope)) / Number(warpUp(second,scope));
        case '+':
            out = Number(warpUp(first,scope)) + Number(warpUp(second,scope));
            break;
        case '-':
            out = Number(warpUp(first,scope)) - Number(warpUp(second,scope));
            break;
    }
    return Number(warpDown(out,scope));
}

function warpUp(foo, scope){
    let index = foo.indexOf('.');
    let bar = foo;
    if (index === -1) {
        for (let i =0; i < scope;i++)
            bar=bar+'0';
    } else {
        bar = foo.substring(0,index)+foo.substring(index+1);
        let extra=scope-(foo.length-index-1);
        for (let i =0; i < extra;i++)
            bar=bar+'0';
    }
    return bar;
}
function warpDown(foo, scope){
    let bar = String(foo);
    return bar.substring(0,bar.length-scope)+'.'+bar.substring(bar.length-scope);
}

function transcribe(foo) {
    let out = "";
    let found = false;
    for (let i = 0; i < foo.length; i++) {
        found = false;
        for (let key in symbols) {
            if (foo[i] === key) {
                out += symbols[key];
                found = true;
                break;
            } else if (foo[i].toLowerCase() === key) {
                out += "uppercase " + symbols[key];
                found = true;
                break;
            }
        }
        for (let key in symbols2) {
            if (foo[i] === key) {
                out += symbols2[key] + " sign";
                found = true;
                break;
            }
        }
        if (!found)
            out += foo[i];
        out += ", ";
    }
    return "<p>Transcribed: " + clipboard_able_span(out) + "</p>";
}

function visualize_color(foo) {
    let out = "";
    let found_hex = false;
    for (let i in basic_web_colors)
        if (i === foo) {
            out = basic_web_colors[i];
            found_hex = true;
            break;
        }
    if (!found_hex)
        for (let i in colors)
            if (i === foo) {
                out = colors[i];
                found_hex = true;
                break;
            }
    if (!found_hex && !/#[\dabcdefABCEDF]{3}/g.test(foo) && !/#[\dabcdefABCEDF]{6}/g.test(foo)) return "";
    return "<p class='width100' style='background-color: " + (found_hex ? out : foo) + "'>" + (found_hex ? foo + " matches color " + out : "Color " + foo + " looks like this") + "</p>";
}

function check_string_stats(foo) {
    let length = foo.length
    let numbers = (foo.match(/\d/g) || []).length;
    let lowercase = (foo.match(/[a-z]/g) || []).length;
    let uppercase = (foo.match(/[A-Z]/g) || []).length;
    let special = (foo.match(/[^a-zA-Z0-9]/g) || []).length;
    let summary = "; as a password: ";
    if (length < 8)
        summary += " extremely short";
    else if (length < 12)
        summary += " short";
    else if (length < 16)
        summary += " somewhat short";
    else if (length > 32)
        summary += " long";
    else if (length > 64)
        summary += " extremely long";
    else
        summary += " reasonably long";
    summary += ", ";
    let complexity = 0;
    if (numbers > 0) complexity += 1;
    if (lowercase > 0) complexity += 1;
    if (uppercase > 0) complexity += 1;
    if (special > 0) complexity += 1;
    if (complexity < 2)
        summary += "extremely simple";
    else if (complexity < 3)
        summary += "low complexity";
    else if (complexity < 4)
        summary += "moderately complex";
    else
        summary += "complex"
    return "Length: " + length + ", numbers: " + numbers + ", lowercase letters: " + lowercase + ", uppercase letters: "
        + uppercase + ", special characters: " + special + summary;
}

function decimal_to_binary(foo, raw) {
    if (Number.isNaN(Number(foo)) || foo.length > 10) return "";
    let out = "";
    while (foo > 0) {
        out = ((foo % 2 === 0) ? "0" : "1") + out;
        foo = (foo / 2) | 0;
    }
    if (raw || out.length === 0) return out;
    return "<p>In binary: <code class=\"clipboard_able_special\" lang=\"zxx\">" + out + "</code></p>";
}

function binary_to_decimal(foo, raw) {
    if (Number.isNaN(Number(foo)) || !/^[01]+$/.test(foo) || foo.length > 50) return "";
    let out = 0;
    let a = 0;
    for (let i = foo.length - 1; i > -1; i--) {
        if (foo[i] === '1') out += Math.pow(2, a);
        a++;
    }
    if (raw) return out;
    return "<p>In decimal: " + clipboard_able_span(out) + "</p>";
}

let bits = "KMGTPEZY";

function parse_as_size(foo) {
    if (Number.isNaN(Number(foo)))
        return "";
    let temp = Number(foo);
    let parts = [];
    let out = "<p>Assuming it's size in bytes: <span class='clipboard_able_special'>";
    while (temp >= 1024) {
        parts[parts.length] = temp % 1024;
        temp -= parts[parts.length - 1];
        temp /= 1024;
    }
    parts[parts.length] = temp % 1024;
    if (parts.length > bits.length)
        return "<p>Assuming it's size in bytes, more than 1023 YiB</p>";
    for (let i = 1; i < parts.length; i++)
        if (parts[parts.length - i] !== 0)
            out += parts[parts.length - i] + " " + bits[parts.length - i - 1] + "iB ";
    if (parts[0] !== 0)
        out += parts[0] + " B";
    out += "</span></p>";
    return out;
}

function get_factors(foo, raw) {
    if (Number.isNaN(Number(foo)) || foo.length > 6)
        return "";
    let bar = Number(foo);
    if (bar > 99999)
        return "";
    let out = [];
    for (let i = 1; i <= Math.min(bar / 2, 99999); i++)
        if (bar % i === 0)
            out[out.length] = i;
    if (raw)
        return out;
    if (out.length === 0)
        return "";
    return "<p>Factors: " + clipboard_able_span(out.join(", ") + ", " + foo) + "</p>";
}

function binary_to_8(foo) {
    return Array(8 - foo.length).fill(0).join('') + foo;
}

function normalize_binary(foo) {
    let out = "";
    let bar = true;
    for (let i = 0; i < foo.length; i++) {
        if (foo[i] === '1') {
            out += foo[i];
            bar = false;
        } else if (!bar) {
            out += foo[i];
        }
    }
    return out;
}

function string_to_ascii(foo) {
    let out = "";
    let done = false;
    let ok = true;
    for (let i = 0; i < foo.length; i++) {
        for (let key in ascii) {
            if (ascii[key] === foo[i]) {
                out += binary_to_8(decimal_to_binary(key, true));
                done = true;
                break;
            }
        }
        if (!done) {
            ok = false;
            done = false;
        }
    }
    return (!ok
            ? "<p>Couldn't encode entire input in ASCII in binary notation; best effort: "
            : "<p>Encoded in ASCII in binary notation: ")
        + "<code class=\"clipboard_able_special\" lang=\"zxx\">" + out + "</code></p>";
}

function ascii_to_string(foo) {
    let out = "";
    let bar = "";
    for (let i = 0; i < foo.length; i++) {
        if (foo[i] === ' ') {
            out += " ";
            bar = "";
            continue;
        }
        if (Number.isNaN(Number(foo[i])) || !/^[01]+$/.test(foo[i])) return "";
        if (bar.length === 8) {
            let cur = ascii[Number(binary_to_decimal(normalize_binary(bar), true))];
            if (cur !== undefined)
                out += cur;
            bar = "";
        }
        bar += "" + foo[i];
    }
    if (bar.length === 8) {
        let cur = ascii[Number(binary_to_decimal(normalize_binary(bar), true))];
        if (cur !== undefined)
            out += cur;
    }
    return "<p>Text from ASCII in binary notation: " + clipboard_able_span(out) + "</p>";
}

function morse(foo) {
    if (!looks_like_morse(foo))
        encode_morse(foo);
}

function looks_like_morse(foo) {
    return /^[-. ]+$/.test(foo);
}

// . 1 unit
// - 3 units
// space between parts of the same letter is one unit
// space between letters is three units
// space between words is seven units
let morse_table = {
    '1': ".----",
    '2': "..---",
    '3': "...--",
    '4': "....-",
    '5': ".....",
    '6': "-....",
    '7': "--...",
    '8': "---..",
    '9': "----.",
    '0': "-----",
    'a': ".-",
    'b': "-...",
    'c': "-.-.",
    'd': "-..",
    'e': ".",
    'f': "..-.",
    'g': "--.",
    'h': "....",
    'i': "..",
    'j': ".---",
    'k': "-.-",
    'l': ".-..",
    'm': "--",
    'n': "-.",
    'o': "---",
    'p': ".--.",
    'q': "--.-",
    'r': ".-.",
    's': "...",
    't': "-",
    'u': "..-",
    'v': "...-",
    'w': ".--",
    'x': "-..-",
    'y': "-.--",
    'z': "--..",
    '.': ".-.-.-",
    ',': "--..--",
    '?': "..--..",
    '\'': ".---.",
    '!': "-.-.--",
    '//': "-..-.",
    '(': "-.--.",
    ')': ".--.-",
    '&': ".-...",
    ':': "---...",
    ';': "-.-.-.",
    '=': "-...-",
    '+': ".-.-.",
    '-': "-....-",
    '_': "..--.-",
    '"': ".-..-.",
    '$': "...-..-",
    '@': ".--.-.",
    ' ': "  "
};

function decode_morse(foo) {
    document.getElementById("morse_prosigns").classList.add("hidden");
    let out = [];
    let code = "";
    let len = 0;
    for (let i = 0; i < foo.length; i++) {
        if (foo[i] === '.' || foo[i] === '-') {
            len = 0;
            code += foo[i];
        } else if (len < 2) {
            len += 1;
        } else if (code.trim() === "") {
            out[out.length] = ' ';
        } else {
            for (let key in morse_table) {
                if (same_except_case(morse_table[key], code)) {
                    out[out.length] = key;
                    code = "";
                    break;
                }
            }
        }
    }
    if (code !== "") {
        for (let key in morse_table) {
            if (same_except_case(morse_table[key], code)) {
                out[out.length] = key;
                code = "";
                break;
            }
        }
    }
    return "<p>From Morse code: <code class=\"clipboard_able_special\" lang=\"zxx\">" + out.join('') + "</code></p>";
}

function encode_morse(foo) {
    document.getElementById("morse_prosigns").classList.remove("hidden");
    let out = [];
    for (let i = 0; i < foo.length; i++)
        for (let key in morse_table)
            if (same_except_case(key, foo[i])) {
                out[i] = morse_table[key];
                break;
            }
    return "<p>In Morse code: <code class=\"clipboard_able_special preservingSpaces\" lang=\"zxx\">" + out.join("   ") + "</code></p>";
}

analyze(true); // Let's got for it