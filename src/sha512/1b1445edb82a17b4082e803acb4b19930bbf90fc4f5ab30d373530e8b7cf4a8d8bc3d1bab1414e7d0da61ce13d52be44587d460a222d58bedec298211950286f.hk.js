window.onload = hk_load;
window.onscroll = hk_onscroll;
let april1stTimeout = 0;
let now = Date.now();
let window_location_href = "";

function hk_load() {
    try {
        getLocation();
        let legacyOverrides = [
            {legacyPath: "#games_rop,games_rop_north", newPath: "roots_of_pacha.html#", comment: "it's north"},
            {legacyPath: "#games_rop,games_rop_south", newPath: "roots_of_pacha.html#", comment: "it's south"}
        ];
        for (let i = 0; i < legacyOverrides.length; i++) {
            if (window_location_href.indexOf(legacyOverrides[i].legacyPath) !== -1) {
                update_window_location_href(
                    window_location_href.replace(legacyOverrides[i].legacyPath, legacyOverrides[i].newPath),
                    legacyOverrides[i].comment);
                return;
            }
        }
        theme = (window_location_href.indexOf("theme=light") !== -1) ?
            "light"
            : localStorage.hk_theme;
        changeTheme(theme === "light" ? theme : "dark", true);
        let localFontSize = localStorage.hk_font_size;
        if (window_location_href.indexOf("fontSize=") !== -1) {
            let temp = window_location_href.substring(window_location_href.indexOf("fontSize=") + 9, window_location_href.indexOf("fontSize=") + 11);
            if (!Number.isNaN(temp) && Number.isInteger(temp))
                fontSize = Number(temp);
        } else if (localFontSize !== null && !isNaN(localFontSize) && !Number.isNaN(localFontSize))
            fontSize = localFontSize;
        changeFontSize(fontSize, true);
        if (window_location_href.indexOf("root") > -1) {
            setBottomText("This site uses no cookies.");
            if (new Date(now).getDate() === 1 && new Date(now).getMonth() === 3) // Date.getMonth returns 0-based value
                isAprilFirst = true;
            if (isAprilFirst) {
                document.body.style.MozTransform = 'rotate(180deg)';
                document.body.style['-webkit-transform'] = 'rotate(180deg)';
                setBottomText("Happy April Fool's Day!");
            }
        }
        if (window_location_href.indexOf("roots_of_pacha") !== -1)
            handleRopHref(href_after_hash);
        handleHref();
        log_sums();
    } catch (e) {
        console.error("Exception in hk_load", e);
    }
}

function log_sums() {
    let hk_js_src = undefined;
    let hk_css_href = undefined;
    try {
        hk_js_src = document.getElementById("hk_js").src;
        hk_css_href = document.getElementById("hk_css").href;
        let hk_js_parts = hk_js_src.split('/');
        let hk_css_parts = hk_css_href.split('/');
        let sums =
            "Reported SHA512 checksum of hk.js: " + hk_js_parts[hk_js_parts.length - 1].split('.')[0]
            + "\nReported SHA512 checksum of hk.css: " + hk_css_parts[hk_css_parts.length - 1].split('.')[0];
        console.log(sums);
    } catch (e) {
        console.error(
            "Exception in log_sums; hk_js.src == " + hk_js_src + " hk_css.href == " + hk_css_href,
            e, hk_js_src, hk_css_href);
    }
}

let scrollTopButton = document.getElementById("scrollTopButton");
scrollTopButton.addEventListener('click', function () {
    scrollUp();
});

function hk_onscroll() {
    if (scrollTopButton == null) {
        scrollTopButton = document.getElementById("scrollTopButton");
    }
    if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
        document.getElementById("footer").classList.remove("hidden");
        scrollTopButton.classList.remove("hidden");
    } else {
        document.getElementById("footer").classList.add("hidden");
        scrollTopButton.classList.add("hidden");
    }
}

function april1st() {
    document.body.style.MozTransform = 'rotate(360deg)';
    document.body.style['-webkit-transform'] = 'rotate(360deg)';
}

let links = document.getElementsByTagName("a");
for (let i = 0; i < links.length; i++)
    links[i].dataset.href = links[i].href;
let theme = "dark";
let fontSize = 18;
const fontDefaultSize = 18;
let href_after_hash = getValueAfterHash(window_location_href);

function confirmNavigation(url, text) {
    if (confirm(text))
        window.open(url, "_blank");
}

let externalUrls = document.getElementsByClassName("outer_url");
for (let i = 0; i < externalUrls.length; i++) {
    externalUrls[i].removeAttribute("href");
    externalUrls[i].target = "_blank";
    externalUrls[i].addEventListener('click', () =>
        confirmNavigation(
            externalUrls[i].dataset.href,
            "Navigating to external site (in new tab): " + externalUrls[i].dataset.title
            + "\nURL: " + externalUrls[i].dataset.href
            + "\nLast accessed at: " + externalUrls[i].dataset.seen)
    );
}
let to_hide = document.getElementsByClassName("to_hide");
for (let i = 0; i < to_hide.length; i++)
    hideElement(to_hide[i]);
let section_toggles = document.getElementsByClassName("section_toggle");
for (let i = 0; i < section_toggles.length; i++) {
    section_toggles[i].addEventListener('click', function () {
        toggleSection(section_toggles[i])
    });
}
let tab_toggles = document.getElementsByClassName("tab_toggle");
for (let i = 0; i < tab_toggles.length; i++) {
    //tab_toggles[i].href = tab_toggles[i].dataset.target;
    tab_toggles[i].addEventListener('click', function () {
        toggleTab(tab_toggles[i])
    });
}
let to_insert = document.getElementsByClassName("to_insert");
for (let i = 0; i < to_insert.length; i++) {
    let foo = to_insert[i].dataset.html;
    foo = reallyReplace(foo, " ", " ");
    to_insert[i].innerHTML = foo;
}
let clipboard_able = document.getElementsByClassName("clipboard_able");
for (let i = 0; i < clipboard_able.length; i++) {
    let clip = document.createElement('span');
    clip.innerText = '📋';
    clip.classList.add('paddingLeft10px', 'float-right', 'cursorPointer');
    addClipboardEvent(clip, clipboard_able[i]);
    addClipboardEvent(clipboard_able[i]);
    if (clipboard_able[i].classList.contains("float-right"))
        clipboard_able[i].parentNode.insertBefore(clip, clipboard_able[i]);
    else
        clipboard_able[i].parentNode.insertBefore(clip, clipboard_able[i].nextSibling);
}
make_clipboard_able("clipboard_able_subtle");
let clipboard_able_special = make_clipboard_able("clipboard_able_special");

function make_clipboard_able(name) {
    let foo = document.getElementsByClassName(name);
    for (let i = 0; i < foo.length; i++)
        if (foo[i].dataset.text !== null && foo[i].dataset.text !== undefined)
            addClipboardEventInner(foo[i], foo[i].dataset.text);
        else
            addClipboardEvent(foo[i]);
    return foo;
}

function addClipboardEvent(target, data = null) {
    let foo = data == null ? target : data;
    addClipboardEventInner(target, foo.tagName === 'TEXTAREA' ? foo.value : foo.innerText);
}

function addClipboardEventInner(target, data) {
    target.addEventListener('click', function () {
        copyToClipboard(data)
    });
}

let toggleSettings = document.getElementsByClassName("toggleSettings");
let settings_option = document.getElementsByClassName("settings_option");
let settings_visible = false;
for (let i of toggleSettings)
    i.addEventListener("click", function () {
        for (let i = 0; i < settings_option.length; i++) {
            if (settings_visible)
                settings_option[i].classList.add("hidden");
            else
                settings_option[i].classList.remove("hidden");
        }
        settings_visible = !settings_visible;
    });

function updateHref(loading, force) {
    if (!loading) {
        localStorage.hk_theme = theme;
        localStorage.hk_font_size = fontSize;
    }
    if (fontSize !== fontDefaultSize || force) {
        let loc = window_location_href;
        let index = loc.indexOf('?');
        let extra = "?";
        if (index !== -1)
            loc = loc.substring(0, index);
        else
            extra = "#?";
        if (theme !== "dark")
            extra += "theme=light";
        if (fontSize !== fontDefaultSize)
            extra += (extra.length > 1 ? "," : "") + "fontSize=" + fontSize;
        if (otherHrefData !== null && otherHrefData.length > 0 && (otherHrefData !== "north=,south=" || loc.indexOf("roots_of_pacha") !== -1))
            extra += (extra.length > 1 ? "," : "") + otherHrefData;
        update_window_location_href(loc + extra, "updating href");
    }
}

function handleHref() {
    let parts = window_location_href.split('?');
    if (parts.length < 1) return;
    if (parts[0].indexOf("#") !== -1) {
        let target = parts[0].split('#')[1];
        if (target !== "")
            changeDisplayedContent();
    }
    if (parts.length < 2) return;
    changeSettings(parts[1]);
}

function getLocation() {
    window_location_href = window.location.href;
    // href_after_hash = getValueAfterHash(window_location_href);
    // let hash = href_after_hash;
    // if (href_after_hash.indexOf('?') !== -1)
    //     hash = href_after_hash.split('?')[0];
    // location.hash = hash;
}

function getValueAfterHash(input) {
    let index = input.lastIndexOf('#');
    if (index === -1)
        return "";
    return input.substring(index + 1);
}

let selectDarkTheme = document.getElementsByClassName("selectDarkTheme");
for (let i of selectDarkTheme)
    i.addEventListener('click', function () {
        changeTheme("dark", false);
    });
let selectLightTheme = document.getElementsByClassName("selectLightTheme");
for (let i of selectLightTheme)
    i.addEventListener('click', function () {
        changeTheme("light", false);
    });
let increaseFontSize = document.getElementsByClassName("increaseFontSize");
for (let i of increaseFontSize)
    i.addEventListener('click', function () {
        changeFontSize(Number(fontSize) + 2, false);
    });
let defaultFontSize = document.getElementsByClassName("defaultFontSize");
for (let i of defaultFontSize)
    i.addEventListener('click', function () {
        changeFontSize(Number(fontDefaultSize), false);
    });
let reduceFontSize = document.getElementsByClassName("reduceFontSize");
for (let i of reduceFontSize)
    i.addEventListener('click', function () {
        changeFontSize(Number(fontSize) - 2, false);
    });

function changeFontSize(newSize, loading = false) {
    if (Number(newSize) < 12 || Number(newSize) > 50)
        return;
    fontSize = Number(newSize);
    changeElementFontSize(document.getElementById("index-body"), fontSize);
    let foo = document.getElementsByTagName("body");
    for (let i = 0; i < foo.length; i++)
        changeElementFontSize(foo[i], fontSize);
    updateHref(loading, true);
}

function changeElementFontSize(element, newSize) {
    if (newSize !== fontDefaultSize)
        element.style.fontSize = newSize + "px";
    else if (element.style.fontSize !== "" || element.style.fontSize !== undefined)
        element.style.fontSize = "";
}

let themes = ["dark", "light"];

function changeTheme(next, loading = false) {
    theme = next;
    changeThemeInner(next, next === themes[0] ? themes[1] : themes[0]);
    updateHref(loading, loading !== true);
}

function changeThemeInner(next, prev) {
    let prevSelector = "." + prev;
    document.querySelectorAll(prevSelector).forEach(item => {
        item.classList.remove(prev);
        item.classList.add(next);
    });
    document.querySelectorAll('a').forEach(item => {
        if (item.classList.contains(prev))
            item.classList.remove(prev);
        item.classList.add(next);
    });
    document.querySelectorAll('code').forEach(item => {
        if (item.classList.contains(prev))
            item.classList.remove(prev);
        item.classList.add(next);
    });
}

function toggleSection(value) {
    let foo = document.getElementById(getValueAfterHash(value.dataset.target).split(',').pop());
    if (isVisible(foo))
        hideElement(foo);
    else
        showElement(foo);
}

function toggleTab(value) {
    toggleTabInner(value.dataset.tabdiv, value.dataset.target);
}

let timeout = 0;
let isAprilFirst = false;
let bottomMessageSpan = document.getElementById("bottomMessage");

function copyToClipboardFromId(id) {
    let item = document.getElementById(id);
    copyToClipboard(item.value.length > item.innerText.length ? String(item.value) : String(item.innerText));
}

function copyToClipboard(text) {
    let ok = navigator.clipboard.writeText(text);
    let lines = text.split("\n");
    let textLinesCount = lines.length;
    let pre = "";
    let post = "";
    let displayed = "";
    if (!ok) {
        setBottomText("❗ Failed to copy to clipboard:" + pre + " <span class='yellow'>" + displayed + "</span>" + post);
        console.warn("Failed to copy to clipboard", text);
        return;
    }
    if (textLinesCount > 1) {
        pre = " " + textLinesCount + " lines starting with";
        if (lines[0].length > 100) {
            pre += " a line longer than 100 characters starting with:"
            displayed = String(lines[0]).slice(0, 100);
        } else {
            pre += ":"
            displayed = String(lines[0]);
        }
    } else if (text.length > 100) {
        pre = " text longer than 100 characters starting with:";
        displayed = String(text).slice(0, 100);
    } else {
        displayed = String(text);
    }
    setBottomText("❕ Copied to clipboard:" + pre + " <span class='yellow'>" + displayed + "</span>" + post);
    console.info("Copied to clipboard", text);
}

function setBottomText(text) {
    document.getElementById("footer").classList.remove("hidden");
    bottomMessageSpan.style.display = "block";
    bottomMessageSpan.innerHTML = text;
    if (timeout !== 0) {
        clearTimeout(timeout);
    }
    timeout = setTimeout(hideBottomMessageSpan, 10000);
    if (isAprilFirst) {
        if (april1stTimeout !== 0) {
            clearTimeout(april1stTimeout);
        }
        april1stTimeout = setTimeout(april1st, 5000);
    }
}

function hideBottomMessageSpan() {
    bottomMessageSpan.innerHTML = "";
    document.getElementById("footer").classList.add("hidden");
}

function toggleTabInner(tab_div, target) {
    let tabs = document.querySelectorAll("#" + tab_div + " div");
    let bar = getValueAfterHash(target).split('?')[0].split(',').pop();
    let foo = document.getElementById(bar);
    for (let i = 0; i < tabs.length; i++)
        hideElement(tabs[i]);
    showElement(foo);
    updateHref();
}

function show(id) {
    showElement(document.getElementById(id));
}

function hideElement(foo) {
    if (!foo) return;
    foo.classList.add("hidden");
}

function showElement(foo) {
    if (!foo) return;
    foo.classList.remove("hidden");
}

let multi_level_first_fade_in_done = false;

function changeDisplayedContent(names) {
    if (names === null || names === undefined) return;
    let ids = names.split(',');
    if (ids.length === 1) {
        openTab(ids[0]);
    } else if (ids.length > 1) {
        openTab(ids[0], multi_level_first_fade_in_done);
        multi_level_first_fade_in_done = true;
        for (let i = 1; i < ids.length; i++)
            show(ids[i]);
    }
}

function changeSettings(settings) {
    let pairs = settings.split(',');
    for (let i = 0; i < pairs.length; i++) {
        let parts = pairs[i].split('=');
        if (parts.length < 2) continue;
        switch (parts[0]) {
            case "theme":
                if (parts[1] === "dark")
                    changeTheme("dark", true);
                else
                    changeTheme("light", true);
                break;
            case "fontSize":
                changeFontSize(parts[1], true);
                break;
        }
    }
}

const siteTabs = document.getElementsByClassName("tab");
const black_screen = document.getElementById("black_screen");

function openTab(tabName, sharp = false) {
    if (black_screen == null) {
        for (let i = 0; i < siteTabs.length; i++)
            hideElement(siteTabs[i]);
        show(tabName);
    } else {
        black_screen.dataset.tab_name = tabName;
        if (!sharp) {
            if (black_screen.classList.contains("faded")) {
                black_screen.classList.add("hiding");
                black_screen.classList.remove("faded");
            } else {
                for (let i = 0; i < siteTabs.length; i++)
                    hideElement(siteTabs[i]);
                show(tabName);
                black_screen.classList.add("showing");
            }
        } else {
            if (!black_screen.classList.contains("faded"))
                black_screen.classList.add("faded");
            for (let i = 0; i < siteTabs.length; i++)
                hideElement(siteTabs[i]);
            show(tabName);
        }
    }
}

if (black_screen != null)
    black_screen.addEventListener('animationend', () => {
        if (black_screen.classList.contains("hiding")) {
            black_screen.classList.remove("hiding");
            black_screen.classList.add("showing");
            for (let i = 0; i < siteTabs.length; i++)
                hideElement(siteTabs[i]);
            let target = (black_screen.dataset.tab_name !== null && black_screen.dataset.tab_name.length > 1) ?
                black_screen.dataset.tab_name
                : "root";
            show(target);
        } else {
            black_screen.classList.remove("showing");
            black_screen.classList.add("faded");
        }
    });

function reallyReplace(foo, input, output) {
    do {
        foo = foo.replace(input, output);
    } while (foo.indexOf(input) > 0);
    return foo;
}

function isVisible(foo) {
    return !foo.classList.contains("hidden") || foo.style.display === "block";
}

let otherHrefData = "north=,south=";
let prevOtherHrefData = "";

function updateOtherHrefData(name, data) {
    let parts = otherHrefData.split(',');
    let foo = "";
    let done = false;
    if (parts.length === 1 && parts[0] === "")
        foo = name + "=" + data;
    else {
        for (let i = 0; i < parts.length; i++) {
            let bar = parts[i].split('=');
            if (bar.length !== 2) continue;
            if (foo.length !== 0) foo += ",";
            if (bar[0] === name) {
                foo += name + "=" + data;
                done = true;
                continue;
            }
            foo += bar[0] + "=" + bar[1];
        }
        if (!done) {
            foo += "," + name + "=" + data;
        }
    }
    otherHrefData = foo;
    if (otherHrefData === prevOtherHrefData) return;
    prevOtherHrefData = otherHrefData;
    updateHref(false, true);
}

let should_notify_parent_window_on_click = document.getElementsByTagName("a");
for (let i = 0; i < should_notify_parent_window_on_click.length; i++)
    should_notify_parent_window_on_click[i].addEventListener('click', function () {
        parent.postMessage(should_notify_parent_window_on_click[i].href, "*");
    });

function scrollUp() {
    document.documentElement.scrollTop = 0;
    window.location.hash = "#index-body";
}

function clipboard_able_span(foo) {
    return "<span class='clipboard_able_special'>" + foo + "</span>";
}

function encode_to_html(input) {
    let foo = document.createElement('textarea');
    foo.innerText = input;
    let bar = foo.innerHTML;
    bar = reallyReplace(bar, "\"", "&quot;");
    bar = reallyReplace(bar, "'", "&#39;");
    bar = reallyReplace(bar, " ", "&nbsp;");
    return bar;
}

function same_except_case(foo, bar) {
    return foo.localeCompare(bar, undefined, {sensitivity: "accent"}) === 0;
}

function update_window_location_href(value, comment = null) {
    window.location.replace(value);
    window_location_href = value;
    if (comment !== null && comment.length > 0)
        console.log("Updating window.location.href to", value);
}

let get_public_ip = document.getElementById("get_public_ip");
if (get_public_ip !== null)
    get_public_ip.addEventListener(
        "click",
        async () => {
            await getPublicIP();
        },
        false);

async function getPublicIP() {
    try {
        let foo = (await (await fetch("https://hekkaryk.pl/ip")).text());
        alert("Your public IP is:\n" + foo);
        copyToClipboard(foo);
    } catch (e) {
        console.error(e, "Can't fetch public IP");
        confirmNavigation(
            "https://hekkaryk.pl/ip",
            "Open https://hekkaryk.pl/ip to get public IP (in new tab)?");
    }
}